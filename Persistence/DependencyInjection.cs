﻿using Library.Domain.Interfaces;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Persistence
{
	public static class DependencyInjection
	{
		public static IServiceCollection AddLibraryPersistence(this IServiceCollection services, IConfiguration configuration)
		{
			services.AddDbContext<AppDbContext>((sp, options) =>
			{
				options.UseNpgsql(configuration.GetConnectionString("DefaultConnection"));

			});

			services.AddScoped<IApplicationDbContextService>(provider => provider.GetService<AppDbContext>());

			return services;
		}
	}
}
