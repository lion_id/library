﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Library.Inrfrastructure.Persistence.Migrations
{
    public partial class inusermodeladdnewpropertiesforgettingstatususers : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "StatusUser",
                table: "User",
                type: "integer",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "StatusUser",
                table: "User");
        }
    }
}
