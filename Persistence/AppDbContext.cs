﻿using Library.Domain.Interfaces;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Persistence
{
	/// <summary>
	/// Контекст базы данных
	/// </summary>
	public class AppDbContext : DbContext, IApplicationDbContextService
	{
		public AppDbContext(DbContextOptions<AppDbContext> options) : base(options)
		{

		}
		protected override void OnModelCreating(ModelBuilder builder)
		{
			builder.ApplyConfigurationsFromAssembly(typeof(AppDbContext).Assembly);
			base.OnModelCreating(builder);
		}

		public Task<IDbContextTransaction> BeginTransactionAsync(CancellationToken cancellationToken)
		{
			return this.Database.BeginTransactionAsync(cancellationToken);
		}

		public async Task InvokeTransactionAsync(Func<Task> action, CancellationToken cancellationToken = default)
		{
			if (this.Database.CurrentTransaction != null)
			{
				await action();
				return;
			}

			using var transaction = await this.Database.BeginTransactionAsync(cancellationToken);
			try
			{
				await action();
				await transaction.CommitAsync(cancellationToken);
			}
			catch
			{
				await transaction.RollbackAsync(cancellationToken);
				throw;
			}
		}

		public async Task<T> InvokeTransactionAsync<T>(Func<Task<T>> action, CancellationToken cancellationToken = default)
		{
			if (this.Database.CurrentTransaction != null)
			{
				return await action();
			}
			else
			{
				using var transaction = await this.Database.BeginTransactionAsync(cancellationToken);
				try
				{
					var result = await action();
					await transaction.CommitAsync(cancellationToken);
					return result;
				}
				catch
				{
					await transaction.RollbackAsync(cancellationToken);
					throw;
				}
			}
		}


		async Task IApplicationDbContextService.AddAsync<T>(T entity, CancellationToken cancellationToken)
		{
			await AddAsync(entity, cancellationToken);
		}
	}
}
