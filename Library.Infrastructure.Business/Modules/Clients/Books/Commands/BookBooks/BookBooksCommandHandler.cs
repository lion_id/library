﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using Domain.Entities;
using Domain.Entities.Books;
using Library.Domain.Interfaces;
using Library.Infrastructure.Business.Common.Constants;
using Library.Infrastructure.Business.Interfaces;
using Library.Infrastructure.Business.Modules.Librariants.Books.Models;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Library.Infrastructure.Business.Modules.Clients.Books.Commands.BookBooks
{
    /// <summary>
    /// Обработчик для бронирования книг
    /// </summary>
    public class BookBooksCommandHandler : IRequestHandler<BookBooksCommand, IEnumerable<BookDto>>
    {
        private readonly IApplicationDbContextService dbContext;
        private readonly IMapper mapper;
        private readonly ICurrentUserService currentUserService;
        private readonly IEmailSenderService emailSender;
        private readonly ILocalizationService localizationService;
        public BookBooksCommandHandler(IApplicationDbContextService dbContext, IMapper mapper, ICurrentUserService currentUserService, ILocalizationService localizationService = null)
        {
            this.dbContext = dbContext;
            this.mapper = mapper;
            this.currentUserService = currentUserService;
            this.localizationService = localizationService;
        }

        public async Task<IEnumerable<BookDto>> Handle(BookBooksCommand request, CancellationToken cancellationToken)
        {
            var books = new List<BookDto>();
            foreach (var bookBooks in request.BookBooks)
            {
                var book = await dbContext.Set<Book>().FirstOrDefaultAsync(p => p.Id == bookBooks.Id);
                if (await dbContext.Set<Book>().AnyAsync(p => p.Id == bookBooks.Id && p.BookType != Domain.Entities.Books.Enums.BookType.Busy))
                {

                    if (request.TotalBookingPeriod.HasValue)
                    {
                        book.BookingDeadline = request.TotalBookingPeriod.Value;
                    }
                    else
                    {
                        book.BookingDeadline = bookBooks.BookReservationPeriod.Value;
                    }
                    books.Add(mapper.Map<BookDto>(book));
                }
                else
                {
                    var user = await dbContext.Set<User>().FirstOrDefaultAsync(p => p.Id == currentUserService.UserId);
                    await emailSender.SendEmailAsync(user.Email, localizationService[LocalizationKeys.BookKeys.MailingListForFreeBooks], "<i>Наименование книги:</i> " + "<b>" + book.Name + "</b> <br>" +
                                                                                                                                         "<i>Жанр: </i>" + "<b>" + book.Genre + "</b> <br>" +
                                                                                                                                         "<i>Издатель: </i>" + "<b>" + book.Publisher + "</b> <br>" +
                                                                                                                                         "<i>Автор: </i>" + "<b>" + book.Author + "</b> <br>");
                }
            }
            await dbContext.SaveChangesAsync();

            return books;
        }
    }
}
