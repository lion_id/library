﻿using Library.Infrastructure.Business.Modules.Clients.Books.Models;
using Library.Infrastructure.Business.Modules.Librariants.Books.Models;
using MediatR;
using System;
using System.Collections.Generic;

namespace Library.Infrastructure.Business.Modules.Clients.Books.Commands.BookBooks
{
    /// <summary>
    /// Команда для бронирования книг
    /// </summary>
    public class BookBooksCommand : IRequest<IEnumerable<BookDto>>
    {
        /// <summary>
        /// Идентификатор книг
        /// </summary>
        public IEnumerable<BookBooksDto> BookBooks { get; set; }

        /// <summary>
        /// Общий срок брони книги
        /// </summary>
        public DateTime? TotalBookingPeriod { get; set; }

    }
}
