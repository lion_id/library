﻿using Library.Infrastructure.Business.Modules.Clients.Books.Models;
using MediatR;
using System.Collections.Generic;

namespace Library.Infrastructure.Business.Modules.Clients.Books.Commands.RemoveBookReservation
{
    /// <summary>
    /// Команда для снятия брони с книги
    /// </summary>
    public class RemoveBookReservationCommand : IRequest
    {
        public IEnumerable<BookBooksDto> BookBooks { get; set; }
    }
}
