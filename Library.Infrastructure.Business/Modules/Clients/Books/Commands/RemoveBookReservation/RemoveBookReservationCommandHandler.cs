﻿using Domain.Entities.Books;
using Library.Domain.Interfaces;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Threading;
using System.Threading.Tasks;

namespace Library.Infrastructure.Business.Modules.Clients.Books.Commands.RemoveBookReservation
{
    /// <summary>
    /// Обработчик для снятие книг с брони
    /// </summary>
    public class RemoveBookReservationCommandHandler : AsyncRequestHandler<RemoveBookReservationCommand>
    {
        private readonly IApplicationDbContextService dbContext;

        public RemoveBookReservationCommandHandler(IApplicationDbContextService dbContext)
        {
            this.dbContext = dbContext;
        }

        protected override async Task Handle(RemoveBookReservationCommand request, CancellationToken cancellationToken)
        {
            foreach (var bookBooks in request.BookBooks)
            {
                var book = await dbContext.Set<Book>().FirstOrDefaultAsync(p => p.Id == bookBooks.Id);
                book.BookingDeadline = default;
            }
            await dbContext.SaveChangesAsync();
        }
    }
}
