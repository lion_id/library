﻿using Library.Infrastructure.Business.Modules.Librariants.Books.Models;
using MediatR;
using System.Collections.Generic;

namespace Library.Infrastructure.Business.Modules.Clients.Books.Queries.SearchBooksBy
{
    /// <summary>
    /// Запрос для поиска книга по фильтрам 
    /// </summary>
    public class SearchBooksByQuery : IRequest<IEnumerable<BookDto>>
    {
        /// <summary>
        /// Фильтр
        /// </summary>
        public string Filter { get; set; }
    }
}
