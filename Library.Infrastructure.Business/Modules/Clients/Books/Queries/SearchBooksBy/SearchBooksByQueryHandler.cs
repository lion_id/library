﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using Domain.Entities.Books;
using Library.Domain.Interfaces;
using Library.Infrastructure.Business.Modules.Librariants.Books.Models;
using MediatR;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Library.Infrastructure.Business.Modules.Clients.Books.Queries.SearchBooksBy
{
    /// <summary>
    /// Обработчик запроса для поиска книг по фильтрам
    /// </summary>
    public class SearchBooksByQueryHandler : IRequestHandler<SearchBooksByQuery, IEnumerable<BookDto>>
    {
        private readonly IApplicationDbContextService dbContext;
        private readonly IMapper mapper;
        public SearchBooksByQueryHandler(IApplicationDbContextService dbContext, IMapper mapper)
        {
            this.dbContext = dbContext;
            this.mapper = mapper;
        }

        public async Task<IEnumerable<BookDto>> Handle(SearchBooksByQuery request, CancellationToken cancellationToken)
        {
            return await Task.FromResult(dbContext.Set<Book>()
                                                  .Where(p => p.Genre.Contains(request.Filter) ||
                                                              p.Author.Contains(request.Filter) ||
                                                              p.Publisher.Contains(request.Filter))
                                                  .ProjectTo<BookDto>(mapper.ConfigurationProvider));
        }
    }
}
