﻿using System;

namespace Library.Infrastructure.Business.Modules.Clients.Books.Models
{
    /// <summary>
    /// Модель брони с крайним сроком для каждой индивидуально
    /// </summary>
    public class BookBooksDto
    {
        /// <summary>
        /// Идентификатор книги
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// Срок бронирования книги
        /// </summary>
        public DateTime? BookReservationPeriod { get; set; }
    }
}
