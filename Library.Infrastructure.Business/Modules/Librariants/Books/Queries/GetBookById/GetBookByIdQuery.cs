﻿using Library.Infrastructure.Business.Modules.Librariants.Books.Models;
using MediatR;

namespace Library.Infrastructure.Business.Modules.Librariants.Books.Queries.GetBookById
{
    /// <summary>
    /// Запрос на получение книги по идентификатору
    /// </summary>
    public class GetBookByIdQuery : IRequest<BookDto>
    {
        public int Id { get; set; }
    }
}
