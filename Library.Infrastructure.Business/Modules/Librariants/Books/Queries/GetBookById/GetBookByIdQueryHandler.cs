﻿using AutoMapper;
using Domain.Entities.Books;
using Library.Domain.Interfaces;
using Library.Infrastructure.Business.Modules.Librariants.Books.Models;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Threading;
using System.Threading.Tasks;

namespace Library.Infrastructure.Business.Modules.Librariants.Books.Queries.GetBookById
{
    /// <summary>
    /// Обработчик для выполнения запроса на получения книг по идентификатору
    /// </summary>
    public class GetBookByIdQueryHandler : IRequestHandler<GetBookByIdQuery, BookDto>
    {
        private readonly IApplicationDbContextService dbContext;
        private readonly IMapper mapper;
        public GetBookByIdQueryHandler(IApplicationDbContextService dbContext, IMapper mapper)
        {
            this.dbContext = dbContext;
            this.mapper = mapper;
        }

        public async Task<BookDto> Handle(GetBookByIdQuery request, CancellationToken cancellationToken)
        {
            var book = await dbContext.Set<Book>().FirstOrDefaultAsync(p => p.Id == request.Id);
            return mapper.Map<BookDto>(book);
        }
    }
}
