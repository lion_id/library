﻿using Library.Infrastructure.Business.Modules.Librariants.Books.Models;
using MediatR;
using System.Collections.Generic;

namespace Library.Infrastructure.Business.Modules.Librariants.Books.Queries.GetAllBooks
{
    /// <summary>
    /// Запрос для получения всех книг
    /// </summary>
    public class GetAllBooksQuery : IRequest<IEnumerable<BookDto>>
    {
    }
}
