﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using Domain.Entities.Books;
using Library.Domain.Interfaces;
using Library.Infrastructure.Business.Modules.Librariants.Books.Models;
using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Library.Infrastructure.Business.Modules.Librariants.Books.Queries.GetAllBooks
{
    /// <summary>
    /// Обработчик для выполнения запросов на получение всех книг
    /// </summary>
    public class GetAllBooksQueryHandler : IRequestHandler<GetAllBooksQuery, IEnumerable<BookDto>>
    {
        private readonly IApplicationDbContextService dbContext;
        private readonly IMapper mapper;
        public GetAllBooksQueryHandler(IApplicationDbContextService dbContext, IMapper mapper)
        {
            this.dbContext = dbContext;
            this.mapper = mapper;
        }

        public async Task<IEnumerable<BookDto>> Handle(GetAllBooksQuery request, CancellationToken cancellationToken)
        {
            return await Task.FromResult(dbContext.Set<Book>().ProjectTo<BookDto>(mapper.ConfigurationProvider));
        }
    }
}
