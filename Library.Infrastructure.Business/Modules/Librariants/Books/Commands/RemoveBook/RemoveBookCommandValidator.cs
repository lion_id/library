﻿using Domain.Entities;
using FluentValidation;
using Library.Domain.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace Library.Infrastructure.Business.Modules.Librariants.Books.Commands.RemoveBook
{
    /// <summary>
    /// Валидатор для удаление книги из системы
    /// </summary>
    public class RemoveBookCommandValidator : AbstractValidator<RemoveBookCommand>
    {
        public RemoveBookCommandValidator(IApplicationDbContextService dbContext, ICurrentUserService currentUserService)
        {
            RuleFor(p => p)
                .Must(p => currentUserService.IsLibrariant());

            RuleFor(p => p.Id)
               .NotEmpty()
               .MustAsync(async (id, ct) => await dbContext.Set<User>().AnyAsync(p => p.Id == id));
        }
    }
}
