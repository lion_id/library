﻿using Domain.Entities.Books;
using Library.Domain.Interfaces;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Threading;
using System.Threading.Tasks;

namespace Library.Infrastructure.Business.Modules.Librariants.Books.Commands.RemoveBook
{
    /// <summary>
    /// Обработчик для удаление книги
    /// </summary>
    public class RemoveBookCommandHandler : AsyncRequestHandler<RemoveBookCommand>
    {
        private readonly IApplicationDbContextService dbContext;
        public RemoveBookCommandHandler(IApplicationDbContextService dbContext)
        {
            this.dbContext = dbContext;
        }

        protected override async Task Handle(RemoveBookCommand request, CancellationToken cancellationToken)
        {
            var book = await dbContext.Set<Book>().FirstOrDefaultAsync(p => p.Id == request.Id);
            dbContext.Set<Book>().Remove(book);
            await dbContext.SaveChangesAsync();
        }
    }
}
