﻿using MediatR;

namespace Library.Infrastructure.Business.Modules.Librariants.Books.Commands.RemoveBook
{
    /// <summary>
    /// Команда для удаление книги
    /// </summary>
    public class RemoveBookCommand : IRequest
    {
        public int Id { get; set; }
    }
}
