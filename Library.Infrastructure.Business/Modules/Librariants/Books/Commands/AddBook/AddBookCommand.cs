﻿using Library.Domain.Entities.Books.Enums;
using Library.Infrastructure.Business.Modules.Librariants.Books.Models;
using MediatR;
using System;

namespace Library.Infrastructure.Business.Modules.Librariants.Books.Commands.AddBook
{
    /// <summary>
    /// Команда для добавление книги в систему
    /// </summary>
    public class AddBookCommand : IRequest<BookDto>
    {
        /// <summary>
        /// Наименование книги
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Автор книги
        /// </summary>
        public string Author { get; set; }
        /// <summary>
        /// Жанр
        /// </summary>
        public string Genre { get; set; }
        /// <summary>
        /// Издатель
        /// </summary>
        public string Publisher { get; set; }
        /// <summary>
        /// Комментраий
        /// </summary>
        public string Comment { get; set; }
        /// <summary>
        /// Тип книги
        /// </summary>
        public BookType BookType { get; set; }
    }
}
