﻿using AutoMapper;
using Domain.Entities.Books;
using Library.Domain.Interfaces;
using Library.Infrastructure.Business.Modules.Librariants.Books.Models;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace Library.Infrastructure.Business.Modules.Librariants.Books.Commands.AddBook
{
    /// <summary>
    /// Обработчик для добавления книги в систему
    /// </summary>
    public class AddBookCommandHandler : IRequestHandler<AddBookCommand, BookDto>
    {
        private readonly IApplicationDbContextService dbContext;
        private readonly IMapper mapper;
        private readonly ICurrentUserService currentUserService;
        public AddBookCommandHandler(IApplicationDbContextService dbContext, IMapper mapper, ICurrentUserService currentUserService)
        {
            this.dbContext = dbContext;
            this.mapper = mapper;
            this.currentUserService = currentUserService;
        }

        public async Task<BookDto> Handle(AddBookCommand request, CancellationToken cancellationToken)
        {
            var book = mapper.Map<Book>(request);

            await dbContext.AddAsync(book, cancellationToken);
            await dbContext.SaveChangesAsync(cancellationToken);

            return mapper.Map<BookDto>(book);
        }
    }
}
