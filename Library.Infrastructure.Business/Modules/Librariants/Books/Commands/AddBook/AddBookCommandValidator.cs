﻿using FluentValidation;
using Library.Domain.Interfaces;

namespace Library.Infrastructure.Business.Modules.Librariants.Books.Commands.AddBook
{
    /// <summary>
    /// Валидатор для добавление книги
    /// </summary>
    public class AddBookCommandValidator : AbstractValidator<AddBookCommand>
    {
        public AddBookCommandValidator(ICurrentUserService currentUserService)
        {
            RuleFor(p => p)
                .Must(p => currentUserService.IsLibrariant());

            RuleFor(p => p.Name)
                .NotEmpty();

            RuleFor(p => p.Publisher)
                .NotEmpty();

            RuleFor(p => p.Genre)
                .NotEmpty();

            RuleFor(p => p.Author)
                .NotEmpty();
        }
    }
}
