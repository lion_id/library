﻿using AutoMapper;
using Domain.Entities.Books;
using Library.Domain.Interfaces;
using Library.Infrastructure.Business.Modules.Librariants.Books.Models;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Threading;
using System.Threading.Tasks;

namespace Library.Infrastructure.Business.Modules.Librariants.Books.Commands.UpdateBook
{
    /// <summary>
    /// Обработчик для обновления книги
    /// </summary>
    public class UpdateBookCommandHandler : IRequestHandler<UpdateBookCommand, BookDto>
    {
        private readonly IApplicationDbContextService dbContext;
        private readonly IMapper mapper;
        public UpdateBookCommandHandler(IApplicationDbContextService dbContext, IMapper mapper)
        {
            this.dbContext = dbContext;
            this.mapper = mapper;
        }

        public async Task<BookDto> Handle(UpdateBookCommand request, CancellationToken cancellationToken)
        {
            var book = await dbContext.Set<Book>().FirstOrDefaultAsync(p => p.Id == request.Id);
            mapper.Map(request, book);
            await dbContext.SaveChangesAsync();
            return mapper.Map<BookDto>(request);
        }
    }
}
