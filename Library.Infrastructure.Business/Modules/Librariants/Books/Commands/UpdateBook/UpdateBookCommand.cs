﻿using Library.Domain.Entities.Books.Enums;
using Library.Infrastructure.Business.Modules.Librariants.Books.Models;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Infrastructure.Business.Modules.Librariants.Books.Commands.UpdateBook
{
    /// <summary>
    /// Команда для обновление книги
    /// </summary>
    public class UpdateBookCommand : IRequest<BookDto>
    {
        public int Id { get; set; }
        /// <summary>
        /// Наименование книги
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Автор книги
        /// </summary>
        public string Author { get; set; }
        /// <summary>
        /// Жанр
        /// </summary>
        public string Genre { get; set; }
        /// <summary>
        /// Издатель
        /// </summary>
        public string Publisher { get; set; }
        /// <summary>
        /// Комментраий
        /// </summary>
        public string Comment { get; set; }
        /// <summary>
        /// Тип книги
        /// </summary>
        public BookType BookType { get; set; }
    }
}
