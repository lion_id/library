﻿using FluentValidation;
using Library.Domain.Interfaces;

namespace Library.Infrastructure.Business.Modules.Librariants.Books.Commands.UpdateBook
{
    /// <summary>
    /// Валидатор для обновление книги
    /// </summary>
    public class UpdateBookCommandValidator : AbstractValidator<UpdateBookCommand>
    {
        public UpdateBookCommandValidator(ICurrentUserService currentUserService)
        {
            RuleFor(p => p)
               .Must(p => currentUserService.IsLibrariant());

            RuleFor(p => p.Name)
               .NotEmpty();

            RuleFor(p => p.Publisher)
                .NotEmpty();

            RuleFor(p => p.Genre)
                .NotEmpty();

            RuleFor(p => p.Author)
                .NotEmpty();
        }
    }
}
