﻿using AutoMapper;
using Domain.Entities.Books;
using Library.Domain;
using Library.Domain.Entities.Books.Enums;
using Library.Infrastructure.Business.Modules.Librariants.Books.Commands.AddBook;
using System;

namespace Library.Infrastructure.Business.Modules.Librariants.Books.Models
{
    /// <summary>
    /// ДТО Книга
    /// </summary>
    public class BookDto : Profile
    {
        public BookDto()
        {
            CreateMap<Book, BookDto>()
                .ForMember(x => x.DateAdded, p => p.MapFrom(p => SystemTime.Now()))
                .ReverseMap();

            CreateMap<AddBookCommand, Book>();
        }
        public int Id { get; set; }
        /// <summary>
        /// Наименование книги
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Автор книги
        /// </summary>
        public string Author { get; set; }
        /// <summary>
        /// Жанр
        /// </summary>
        public string Genre { get; set; }
        /// <summary>
        /// Издатель
        /// </summary>
        public string Publisher { get; set; }
        /// <summary>
        /// Дата добавления
        /// </summary>
        public DateTime DateAdded { get; set; }
        /// <summary>
        /// Комментраий
        /// </summary>
        public string Comment { get; set; }
        /// <summary>
        /// Тип книги
        /// </summary>
        public BookType BookType { get; set; }
        /// <summary>
        /// Крайний срок брони
        /// </summary>
        public DateTime BookingDeadline { get; set; }
        /// <summary>
        /// Оценка
        /// </summary>
        public double Evaluation { get; set; }
        /// <summary>
        /// Количество голосов
        /// </summary>
        public int NumberOfVotes { get; set; }

    }
}
