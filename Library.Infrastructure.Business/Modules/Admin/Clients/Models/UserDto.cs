﻿using AutoMapper;
using Domain.Entities;
using Library.Domain.Entities;
using Library.Domain.Entities.Enums;
using Library.Infrastructure.Business.Modules.Admin.Clients.Commands.CreateUser;
using Library.Infrastructure.Business.Modules.Admin.Clients.Commands.UpdateUser;

namespace Library.Infrastructure.Business.Modules.Admin.Clients.Models
{
    public class UserDto : Profile
    {
        public UserDto()
        {
            CreateMap<User, UserDto>()
                .ReverseMap();

            CreateMap<CreateUserCommand, User>();
            CreateMap<UpdateUserCommand, User>();
        }
        public int Id { get; set; }
        /// <summary>
        /// Имя
        /// </summary>
        public string FirstName { get; set; }
        /// <summary>
        /// Фамилия
        /// </summary>
        public string LastName { get; set; }
        /// <summary>
        /// Отчество
        /// </summary>
        public string MiddleName { get; set; }
        /// <summary>
        /// Почта
        /// </summary>
        public string Email { get; set; }
        /// <summary>
        /// Телефонный номер
        /// </summary>
        public string Phone { get; set; }
        /// <summary>
        /// Логин
        /// </summary>
        public string UserName { get; set; }
        public Role Role { get; set; }
        /// <summary>
        /// Статус пользователя
        /// </summary>
        public StatusUser StatusUser { get; set; }
    }
}
