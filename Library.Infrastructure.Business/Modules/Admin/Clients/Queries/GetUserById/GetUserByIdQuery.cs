﻿using Library.Infrastructure.Business.Modules.Admin.Clients.Models;
using MediatR;

namespace Library.Infrastructure.Business.Modules.Admin.Clients.Queries.GetUserById
{
    /// <summary>
    /// Запрос на получение пользователя по идентификатору
    /// </summary>
    public class GetUserByIdQuery : IRequest<UserDto>
    {
        public GetUserByIdQuery(int id)
        {
            Id = id;
        }

        public int Id { get; }
    }
}
