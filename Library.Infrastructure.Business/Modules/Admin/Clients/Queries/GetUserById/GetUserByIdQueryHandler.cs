﻿using AutoMapper;
using Domain.Entities;
using Library.Domain.Interfaces;
using Library.Infrastructure.Business.Modules.Admin.Clients.Models;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Threading;
using System.Threading.Tasks;

namespace Library.Infrastructure.Business.Modules.Admin.Clients.Queries.GetUserById
{
    /// <summary>
    /// Обработчик на получение пользователя по идентификатору
    /// </summary>
    public class GetUserByIdQueryHandler : IRequestHandler<GetUserByIdQuery, UserDto>
    {
        private readonly IApplicationDbContextService dbContext;

        private readonly IMapper mapper;
        public GetUserByIdQueryHandler(IApplicationDbContextService dbContext, IMapper mapper)
        {
            this.dbContext = dbContext;
            this.mapper = mapper;
        }

        public async Task<UserDto> Handle(GetUserByIdQuery request, CancellationToken cancellationToken)
        {
            var user = await dbContext.Set<User>().FirstOrDefaultAsync(p => p.Id == request.Id);
            return mapper.Map<UserDto>(user);
        }
    }
}
