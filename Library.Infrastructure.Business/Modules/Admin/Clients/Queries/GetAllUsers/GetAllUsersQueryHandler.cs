﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using Domain.Entities;
using Library.Domain.Interfaces;
using Library.Infrastructure.Business.Modules.Admin.Clients.Models;
using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Library.Infrastructure.Business.Modules.Admin.Clients.Queries.GetAllUsers
{
    /// <summary>
    /// Обработчик на получение всех пользователей
    /// </summary>
    public class GetAllUsersQueryHandler : IRequestHandler<GetAllUsersQuery, IEnumerable<UserDto>>
    {
        private readonly IApplicationDbContextService dbContext;
        private readonly IMapper mapper;
        public GetAllUsersQueryHandler(IApplicationDbContextService dbContext, IMapper mapper)
        {
            this.dbContext = dbContext;
            this.mapper = mapper;
        }

        public async Task<IEnumerable<UserDto>> Handle(GetAllUsersQuery request, CancellationToken cancellationToken)
        {
            return await Task.FromResult(dbContext.Set<User>().ProjectTo<UserDto>(mapper.ConfigurationProvider));
        }
    }
}
