﻿using Library.Infrastructure.Business.Modules.Admin.Clients.Models;
using MediatR;
using System.Collections.Generic;

namespace Library.Infrastructure.Business.Modules.Admin.Clients.Queries.GetAllUsers
{
    /// <summary>
    /// Запрос на получение всех пользователей
    /// </summary>
    public class GetAllUsersQuery : IRequest<IEnumerable<UserDto>>
    {

    }
}
