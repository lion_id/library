﻿using Domain.Entities;
using FluentValidation;
using Library.Domain.Interfaces;
using Library.Infrastructure.Business.Common.Constants;
using Library.Infrastructure.Business.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Threading;
using System.Threading.Tasks;

namespace Library.Infrastructure.Business.Modules.Admin.Clients.Commands.CreateUser
{
    public class CreateUserCommandValidator : AbstractValidator<CreateUserCommand>
    {
        private readonly IApplicationDbContextService dbContext;

        public CreateUserCommandValidator(IApplicationDbContextService dbContext, ILocalizationService localizationService)
        {
            this.dbContext = dbContext;

            CascadeMode = CascadeMode.StopOnFirstFailure;

            RuleFor(p => p.Email)
                .NotEmpty()
                .WithMessage(localizationService[LocalizationKeys.UserKeys.EmailShouldNotBeEmpty])
                .EmailAddress()
                .WithMessage(localizationService[LocalizationKeys.UserKeys.InvalidFormatEmail])
                .MustAsync(UniqEmail)
                .WithMessage(localizationService[LocalizationKeys.UserKeys.EmailMustBeUniq]);

            RuleFor(user => user)
                .Must(p => p.Password == p.ConfirmPassword)
                .WithMessage(localizationService[LocalizationKeys.UserKeys.PasswordsDontMatch]);

            RuleFor(user => user.FirstName).NotEmpty();

            RuleFor(p => p.UserName)
                .NotEmpty()
                .MustAsync(async (userName, ct) => await dbContext.Set<User>().AllAsync(p => p.UserName != userName, ct))
                .WithMessage(localizationService[LocalizationKeys.UserKeys.UserNameAlreadyExists]);
        }

        private async Task<bool> UniqEmail(string email, CancellationToken ct)
        => await dbContext.Set<User>().AllAsync(p => p.Email != email, ct);

    }
}
