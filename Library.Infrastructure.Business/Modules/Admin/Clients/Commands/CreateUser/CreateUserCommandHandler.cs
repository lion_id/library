﻿using AutoMapper;
using Domain.Entities;
using Library.Domain.Interfaces;
using Library.Infrastructure.Business.Common.Constants;
using Library.Infrastructure.Business.Modules.Admin.Clients.Models;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace Library.Infrastructure.Business.Modules.Admin.Clients.Commands.CreateUser
{
    /// <summary>
    /// Обработчик для создания пользователя
    /// </summary>
    public class CreateUserCommandHandler : IRequestHandler<CreateUserCommand, UserDto>
    {
        private readonly IApplicationDbContextService dbContext;
        private readonly IIdentityService identityService;
        private readonly IEmailSenderService emailSender;
        private readonly IMapper mapper;
        public CreateUserCommandHandler(IApplicationDbContextService dbContext, IIdentityService identityService, IEmailSenderService emailSender, IMapper mapper)
        {
            this.dbContext = dbContext;
            this.identityService = identityService;
            this.emailSender = emailSender;
            this.mapper = mapper;
        }

        public async Task<UserDto> Handle(CreateUserCommand request, CancellationToken cancellationToken)
        {
            return await dbContext.InvokeTransactionAsync(async () =>
            {
                var user = mapper.Map<User>(request);
                user.UserId = await identityService.CreateUserAsync(request.UserName, request.Email, request.Password, false);
                await dbContext.AddAsync(user);
                await dbContext.SaveChangesAsync();

                await identityService.SetRole(user.UserId, request.Role.ToString());
                await identityService.AddToClaimAsync(user.UserId, UserClaimType.UserIdentifier, user.Id, cancellationToken);
                //await emailSender.SendEmailAsync(request.Email, "Пароль от сервиса Библиотека", "<i>Логин:</i> " + "<b>" + request.UserName + "</b> <br>" +
                //                                                                                "<i>Пароль: </i>" + "<b>" + request.Password + "</b> <br>");

                return mapper.Map<UserDto>(user);
            });
        }
    }
}
