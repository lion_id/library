﻿using Library.Domain.Entities;
using Library.Infrastructure.Business.Modules.Admin.Clients.Models;
using MediatR;

namespace Library.Infrastructure.Business.Modules.Admin.Clients.Commands.CreateUser
{
    public class CreateUserCommand : IRequest<UserDto>
    {
        public string FirstName { get; set; }
        /// <summary>
        /// Фамилия
        /// </summary>
        public string LastName { get; set; }
        /// <summary>
        /// Отчество
        /// </summary>
        public string MiddleName { get; set; }
        /// <summary>
        /// Почта
        /// </summary>
        public string Email { get; set; }
        /// <summary>
        /// Телефонный номер
        /// </summary>
        public string Phone { get; set; }
        /// <summary>
        /// Логин
        /// </summary>
        public string UserName { get; set; }
        /// <summary>
        /// Пароль
        /// </summary>
        public string Password { get; set; }
        /// <summary>
        /// Подтверждение пароля
        /// </summary>
        public string ConfirmPassword { get; set; }
        /// <summary>
        /// Роль
        /// </summary>
        public Role Role { get; set; }
    }
}
