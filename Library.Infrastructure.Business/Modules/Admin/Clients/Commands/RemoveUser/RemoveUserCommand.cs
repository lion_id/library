﻿using MediatR;

namespace Library.Infrastructure.Business.Modules.Admin.Clients.Commands.RemoveUser
{
    public class RemoveUserCommand : IRequest
    {
        public RemoveUserCommand(int id)
        {
            Id = id;
        }

        public int Id { get; }
    }
}
