﻿using Domain.Entities;
using FluentValidation;
using Library.Domain.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace Library.Infrastructure.Business.Modules.Admin.Clients.Commands.RemoveUser
{
    public class RemoveUserCommandValidator : AbstractValidator<RemoveUserCommand>
    {
        public RemoveUserCommandValidator(IApplicationDbContextService dbContext)
        {
            RuleFor(p => p.Id)
                .NotEmpty()
                .MustAsync(async (id, ct) => await dbContext.Set<User>().AnyAsync(p => p.Id == id));
        }
    }
}
