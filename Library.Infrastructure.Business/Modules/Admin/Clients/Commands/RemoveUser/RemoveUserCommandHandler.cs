﻿using Domain.Entities;
using Library.Domain.Interfaces;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Threading;
using System.Threading.Tasks;

namespace Library.Infrastructure.Business.Modules.Admin.Clients.Commands.RemoveUser
{
    /// <summary>
    /// Обработчик для удаления пользователя из системы
    /// </summary>
    public class RemoveUserCommandHandler : AsyncRequestHandler<RemoveUserCommand>
    {
        private readonly IApplicationDbContextService dbContext;
        private readonly IIdentityService identityService;
        public RemoveUserCommandHandler(IApplicationDbContextService dbContext, IIdentityService identityService)
        {
            this.dbContext = dbContext;
            this.identityService = identityService;
        }

        protected override async Task Handle(RemoveUserCommand request, CancellationToken cancellationToken)
        {
            var user = await dbContext.Set<User>().FirstOrDefaultAsync(p => p.Id == request.Id);
            await identityService.RemoveUser(user.UserId);
            dbContext.Set<User>().Remove(user);
            await dbContext.SaveChangesAsync();
        }
    }
}
