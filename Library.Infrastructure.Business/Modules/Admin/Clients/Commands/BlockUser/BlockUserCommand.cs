﻿using MediatR;
using System;

namespace Library.Infrastructure.Business.Modules.Admin.Clients.Commands.BlockUser
{
    public class BlockUserCommand : IRequest
    {
        public BlockUserCommand(int id)
        {
            Id = id;
        }

        public int Id { get; }
        public DateTime DateBlocking { get; set; }
    }
}
