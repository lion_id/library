﻿using Domain.Entities;
using Library.Domain.Interfaces;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Threading;
using System.Threading.Tasks;

namespace Library.Infrastructure.Business.Modules.Admin.Clients.Commands.BlockUser
{
    /// <summary>
    /// Обработчик для блокировки пользователя
    /// </summary>
    public class BlockUserCommandHandler : AsyncRequestHandler<BlockUserCommand>
    {
        private readonly IApplicationDbContextService dbContext;
        private readonly IIdentityService identityService;
        public BlockUserCommandHandler(IApplicationDbContextService dbContext, IIdentityService identityService)
        {
            this.dbContext = dbContext;
            this.identityService = identityService;
        }

        protected override async Task Handle(BlockUserCommand request, CancellationToken cancellationToken)
        {
            var user = await dbContext.Set<User>().FirstOrDefaultAsync(p => p.Id == request.Id);
            user.StatusUser = Domain.Entities.Enums.StatusUser.Disable;
            await identityService.LockUserAsync(user.UserId, request.DateBlocking);
            await dbContext.SaveChangesAsync();
        }
    }
}
