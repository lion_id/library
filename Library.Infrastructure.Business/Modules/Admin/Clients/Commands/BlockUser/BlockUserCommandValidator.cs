﻿using Domain.Entities;
using FluentValidation;
using Library.Domain;
using Library.Domain.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;

namespace Library.Infrastructure.Business.Modules.Admin.Clients.Commands.BlockUser
{
    /// <summary>
    /// Валидатор для блокировки пользователя
    /// </summary>
    public class BlockUserCommandValidator : AbstractValidator<BlockUserCommand>
    {
        public BlockUserCommandValidator(IApplicationDbContextService dbContext)
        {
            RuleFor(p => p.Id)
                .NotEmpty()
                .MustAsync(async (id, ct) => await dbContext.Set<User>().AnyAsync(p => p.Id == id));

            RuleFor(p => p.DateBlocking)
                .NotNull()
                .Must(dateBlocking => dateBlocking >= SystemTime.Now());
        }
    }
}
