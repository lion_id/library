﻿using Domain.Entities;
using FluentValidation;
using Library.Domain.Interfaces;
using Library.Infrastructure.Business.Common.Constants;
using Library.Infrastructure.Business.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace Library.Infrastructure.Business.Modules.Admin.Clients.Commands.ChangePassword
{
    /// <summary>
    /// Валидатор для изменения пароля пользователя
    /// </summary>
    public class ChangePasswordCommandValidator : AbstractValidator<ChangePasswordCommand>
    {
        public ChangePasswordCommandValidator(IApplicationDbContextService dbContext, ILocalizationService localizationService)
        {
            RuleFor(p => p.Id)
              .NotEmpty()
              .MustAsync(async (id, ct) => await dbContext.Set<User>().AnyAsync(p => p.Id == id));

            RuleFor(user => user)
                .Must(p => p.Password == p.ConfirmPassword)
                .WithMessage(localizationService[LocalizationKeys.UserKeys.PasswordsDontMatch]);
        }
    }
}
