﻿using MediatR;

namespace Library.Infrastructure.Business.Modules.Admin.Clients.Commands.ChangePassword
{
    /// <summary>
    /// Команда для смены пароля
    /// </summary>
    public class ChangePasswordCommand : IRequest
    {
        public int Id { get; set; }
        public string Password { get; set; }
        public string ConfirmPassword { get; set; }
    }
}
