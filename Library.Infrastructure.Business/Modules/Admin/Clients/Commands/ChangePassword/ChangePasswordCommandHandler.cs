﻿using Domain.Entities;
using Library.Domain.Interfaces;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Threading;
using System.Threading.Tasks;

namespace Library.Infrastructure.Business.Modules.Admin.Clients.Commands.ChangePassword
{
    /// <summary>
    /// Обработчик для смены пароля
    /// </summary>
    public class ChangePasswordCommandHandler : AsyncRequestHandler<ChangePasswordCommand>
    {
        private readonly IApplicationDbContextService dbContext;
        private readonly IIdentityService identityService;
        public ChangePasswordCommandHandler(IApplicationDbContextService dbContext, IIdentityService identityService)
        {
            this.dbContext = dbContext;
            this.identityService = identityService;
        }

        protected override async Task Handle(ChangePasswordCommand request, CancellationToken cancellationToken)
        {
            var user = await dbContext.Set<User>().FirstOrDefaultAsync(p => p.Id == request.Id);
            await identityService.ChangeUserPassword(user.UserId, request.Password);

        }
    }
}
