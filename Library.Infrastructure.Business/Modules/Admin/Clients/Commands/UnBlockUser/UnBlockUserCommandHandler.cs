﻿using Domain.Entities;
using Library.Domain.Interfaces;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Threading;
using System.Threading.Tasks;

namespace Library.Infrastructure.Business.Modules.Admin.Clients.Commands.UnBlockUser
{
    /// <summary>
    /// Обработчик для разблокировки пользователя
    /// </summary>
    public class UnBlockUserCommandHandler : AsyncRequestHandler<UnBlockUserCommand>
    {
        private readonly IApplicationDbContextService dbContext;
        private readonly IIdentityService identityService;
        public UnBlockUserCommandHandler(IApplicationDbContextService dbContext, IIdentityService identityService)
        {
            this.dbContext = dbContext;
            this.identityService = identityService;
        }

        protected override async Task Handle(UnBlockUserCommand request, CancellationToken cancellationToken)
        {
            var user = await dbContext.Set<User>().FirstOrDefaultAsync(p => p.Id == request.Id);
            await identityService.UnlockUserAsync(user.UserId);

        }
    }
}
