﻿using MediatR;

namespace Library.Infrastructure.Business.Modules.Admin.Clients.Commands.UnBlockUser
{
    /// <summary>
    /// Разблокировка пользователя
    /// </summary>
    public class UnBlockUserCommand : IRequest
    {
        public int Id { get; set; }
    }
}
