﻿using Domain.Entities;
using FluentValidation;
using Library.Domain.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace Library.Infrastructure.Business.Modules.Admin.Clients.Commands.UnBlockUser
{
    /// <summary>
    /// Валидатор для разблокировки пользователя
    /// </summary>
    public class UnBlockUserCommandValidator : AbstractValidator<UnBlockUserCommand>
    {
        public UnBlockUserCommandValidator(IApplicationDbContextService dbContext)
        {
            RuleFor(p => p.Id)
                .NotEmpty()
                .MustAsync(async (id, ct) => await dbContext.Set<User>().AnyAsync(p => p.Id == id));
        }
    }
}
