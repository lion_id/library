﻿using Library.Domain.Entities;
using Library.Domain.Entities.Enums;
using Library.Infrastructure.Business.Modules.Admin.Clients.Models;
using MediatR;

namespace Library.Infrastructure.Business.Modules.Admin.Clients.Commands.UpdateUser
{
    public class UpdateUserCommand : IRequest<UserDto>
    {

        /// <summary>
        /// Идентификатор
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// Имя
        /// </summary>
        public string FirstName { get; set; }
        /// <summary>
        /// Фамилия
        /// </summary>
        public string LastName { get; set; }
        /// <summary>
        /// Отчество
        /// </summary>
        public string MiddleName { get; set; }
        /// <summary>
        /// Почта
        /// </summary>
        public string Email { get; set; }
        /// <summary>
        /// Телефонный номер
        /// </summary>
        public string Phone { get; set; }
        /// <summary>
        /// Пароль
        /// </summary>
        public string Password { get; set; }
        /// <summary>
        /// Подтверждение пароля
        /// </summary>
        public string ConfirmPassword { get; set; }
        /// <summary>
        /// Логин
        /// </summary>
        public string UserName { get; set; }
        /// <summary>
        /// Роль
        /// </summary>
        public Role Role { get; set; }
        /// <summary>
        /// Статус пользователя
        /// </summary>
        public StatusUser StatusUser { get; set; }
    }
}
