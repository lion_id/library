﻿using AutoMapper;
using Domain.Entities;
using Library.Domain.Interfaces;
using Library.Infrastructure.Business.Modules.Admin.Clients.Models;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Threading;
using System.Threading.Tasks;

namespace Library.Infrastructure.Business.Modules.Admin.Clients.Commands.UpdateUser
{
    /// <summary>
    /// Обработчик для обновления пароля
    /// </summary>
    public class UpdateUserCommandHandler : IRequestHandler<UpdateUserCommand, UserDto>
    {
        private readonly IApplicationDbContextService dbContext;
        private readonly IIdentityService identityService;
        private readonly IMapper mapper;
        public UpdateUserCommandHandler(IApplicationDbContextService dbContext, IIdentityService identityService, IMapper mapper)
        {
            this.dbContext = dbContext;
            this.identityService = identityService;
            this.mapper = mapper;
        }
        public async Task<UserDto> Handle(UpdateUserCommand request, CancellationToken cancellationToken)
        {
            return await dbContext.InvokeTransactionAsync(async () =>
            {
                var user = await dbContext.Set<User>().FirstOrDefaultAsync(p => p.Id == request.Id);
                var userId = user.UserId;
                mapper.Map(request, user);
                await identityService.UpdateUsernameAsync(userId, request.UserName);
                if(request.Password != default)
                {
                    await identityService.ChangeUserPassword(userId, request.Password);
                }
      
                if (request.StatusUser != user.StatusUser)
                {
                    await identityService.UnlockUserAsync(user.UserId);
                    user.StatusUser = Domain.Entities.Enums.StatusUser.Active;
                }

                await dbContext.SaveChangesAsync();

                await identityService.RemoveFromRole(userId, request.Role.ToString());
                await identityService.SetRole(userId, request.Role.ToString());

                return mapper.Map<UserDto>(user);
            });
        }
    }
}
