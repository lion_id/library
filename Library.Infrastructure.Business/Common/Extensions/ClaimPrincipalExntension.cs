﻿using Library.Domain.Entities;
using Library.Infrastructure.Business.Common.Constants;
using System.Security.Claims;

namespace Library.Infrastructure.Business.Common.Extensions
{
    public static class ClaimPrincipalExntension
    {
        /// <summary>
        /// ИД пользователя
        /// </summary>
        /// <param name="principal"></param>
        /// <returns>Идентификатор</returns>
        public static int UserId(this ClaimsPrincipal principal)
        {
            return int.Parse(principal.FindFirst(p => p.Type == UserClaimType.UserIdentifier)?.Value!);
        }

        /// <summary>
        /// Системный идентификатор пользователя (Identity ID)
        /// </summary>
        /// <param name="principal"></param>
        /// <returns></returns>
        public static string IdentityUserId(this ClaimsPrincipal principal)
        {
            return principal.FindFirst(p => p.Type == ClaimTypes.NameIdentifier)?.Value;
        }

        /// <summary>
        /// Получение роли пользователя
        /// </summary>
        /// <param name="principal"></param>
        /// <returns></returns>
        public static bool IdentityRoleLibrariant(this ClaimsPrincipal principal)
        {
            var role = principal.FindFirst(p => p.Type == ClaimTypes.Role)?.Value;
            if (role == Role.Librarian.ToString())
            {
                return true;
            }
            return false;
        }

        /// <summary>
        /// Получение роли пользователя
        /// </summary>
        /// <param name="principal"></param>
        /// <returns></returns>
        public static bool IdentityRoleAdmin(this ClaimsPrincipal principal)
        {
            var role = principal.FindFirst(p => p.Type == ClaimTypes.Role)?.Value;
            if (role == Role.Admin.ToString())
            {
                return true;
            }
            return false;
        }
    }
}
