﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Infrastructure.Business.Interfaces
{
    public interface ILocalizationService
    {
        string this[string key] { get; }

        string GetResource(string key);
    }
}
