﻿namespace Library.Infrastructure.Business.Common.Constants
{
    /// <summary>
    /// Ключи для локализации
    /// </summary>
    public static class LocalizationKeys
    {
        /// <summary>
        /// Ключи локализации для Сотрудников (USR)
        /// </summary>
        public static class UserKeys
        {
            /// <summary>
            /// Пароли не совпадают
            /// </summary>
            public const string PasswordsDontMatch = "USR_PasswordsDontMatch";
            /// <summary>
            /// Email должен быть уникальным
            /// </summary>
            public const string EmailMustBeUniq = "USR_EmailMustBeUniq";
            /// <summary>
            /// Почта не может быть пустой
            /// </summary>
            public const string EmailShouldNotBeEmpty = "USR_EmailShouldNotBeEmpty";
            /// <summary>
            /// Неверный формат почты
            /// </summary>
            public const string InvalidFormatEmail = "USR_InvalidFormatEmail";
            /// <summary>
            /// Учетная запись с таким именем уже существует
            /// </summary>
            public const string UserNameAlreadyExists = "USR_UserNameAlreadyExists";
            /// <summary>
            /// Время блокировки должно быть больше текущего времени
            /// </summary>
            public const string BlockingTimeMustBeGreaterThanCurrentTime = "USR_BlockingTimeMustBeGreaterThanCurrentTime";

        }

        /// <summary>
        /// Ключи локализации для Книг (BK)
        /// </summary>
        public static class BookKeys
        {
            public const string MailingListForFreeBooks = "BK_MailingListForFreeBooks";
        }
    }
}
