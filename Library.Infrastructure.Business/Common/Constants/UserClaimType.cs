﻿namespace Library.Infrastructure.Business.Common.Constants
{
    /// <summary>
    /// Тип заявления пользователя
    /// </summary>
    public static class UserClaimType
    {
        /// <summary>
        /// Идентификатор пользователя
        /// </summary>
        public const string UserIdentifier = "ИД пользователя";

    }
}
