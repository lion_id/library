﻿namespace Domain.Entities
{
	/// <summary>
	/// Базовый класс для всех сущностей
	/// </summary>
	public abstract class BaseEntity
	{
		public int Id { get; set; }
	}
}
