﻿namespace Library.Domain.Entities.Enums
{
    /// <summary>
    /// Статус пользователя
    /// </summary>
    public enum StatusUser
    {
        Active,
        Disable
    }
}
