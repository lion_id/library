﻿using Library.Domain.Entities.Books.Enums;
using System;

namespace Domain.Entities.Books
{
    /// <summary>
    /// Книга
    /// </summary>
    public class Book : BaseEntity
    {
        /// <summary>
        /// Наименование книги
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Автор книги
        /// </summary>
        public string Author { get; set; }
        /// <summary>
        /// Жанр
        /// </summary>
        public string Genre { get; set; }
        /// <summary>
        /// Издатель
        /// </summary>
        public string Publisher { get; set; }
        /// <summary>
        /// Дата добавления
        /// </summary>
        public DateTime DateAdded { get; set; }
        /// <summary>
        /// Комментраий
        /// </summary>
        public string Comment { get; set; }
        /// <summary>
        /// Тип книги
        /// </summary>
        public BookType BookType { get; set; }
        /// <summary>
        /// Крайний срок брони
        /// </summary>
        public DateTime BookingDeadline { get; set; }
        /// <summary>
        /// Оценка
        /// </summary>
        public double? Evaluation { get; set; }
        /// <summary>
        /// Количество голосов
        /// </summary>
        public int? NumberOfVotes { get; set; }
    }
}
