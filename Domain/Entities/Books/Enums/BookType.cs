﻿namespace Library.Domain.Entities.Books.Enums
{
    /// <summary>
    /// Тип занятости книги
    /// </summary>
    public enum BookType
    {
        /// <summary>
        /// Свободна
        /// </summary>
        Free,
        /// <summary>
        /// Занято
        /// </summary>
        Busy
    }
}
