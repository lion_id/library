﻿using Library.Domain.Entities;
using Library.Domain.Entities.Enums;

namespace Domain.Entities
{
    /// <summary>
    /// Пользователь
    /// </summary>
    public class User : BaseEntity
    {
        /// <summary>
        /// Имя
        /// </summary>
        public string FirstName { get; set; }
        /// <summary>
        /// Фамилия
        /// </summary>
        public string LastName { get; set; }
        /// <summary>
        /// Отчество
        /// </summary>
        public string MiddleName { get; set; }
        /// <summary>
        /// Почта
        /// </summary>
        public string Email { get; set; }
        /// <summary>
        /// Телефонный номер
        /// </summary>
        public string Phone { get; set; }
        /// <summary>
        /// Системный идентификатор (Identity ID)
        /// </summary>
        public string UserId { get; set; }
        /// <summary>
        /// Логин
        /// </summary>
        public string UserName { get; set; }
        public Role Role { get; set; }
        /// <summary>
        /// Статус пользователя
        /// </summary>
        public StatusUser StatusUser { get; set; }
    }
}
