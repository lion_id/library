﻿namespace Library.Domain.Entities
{
    /// <summary>
    /// Роли
    /// </summary>
    public enum Role
    {
        /// <summary>
        /// Администратор
        /// </summary>
        Admin,
        /// <summary>
        /// Библиотекарь
        /// </summary>
        Librarian,
        /// <summary>
        /// Клиент
        /// </summary>
        Client
    }
}
