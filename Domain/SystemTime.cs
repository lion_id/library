﻿using System;

namespace Library.Domain
{
    /// <summary>
    /// Время (Для упрощение тестирования)
    /// </summary>
    public static class SystemTime
    {
        public static Func<DateTime> Now = () => DateTime.Now;
    }
}
