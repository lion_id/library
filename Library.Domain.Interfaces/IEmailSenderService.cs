﻿using System.Threading.Tasks;

namespace Library.Domain.Interfaces
{
	/// <summary>
	/// Интерфейс для отправки почты
	/// </summary>
	public interface IEmailSenderService
	{
		/// <summary>
		/// Отправка почты
		/// </summary>
		/// <param name="email">Почта получателя</param>
		/// <param name="subject">Тема</param>
		/// <param name="htmlMessage">Сообщение</param>
		Task SendEmailAsync(string email, string subject, string htmlMessage);
	}
}
