﻿using System.Threading.Tasks;

namespace Library.Domain.Interfaces
{
    /// <summary>
    /// Получение идентификатора пользователя
    /// </summary>
    public interface ICurrentUserService
    {
        /// <summary>
        /// Идентификатор пользователя
        /// </summary>
        int UserId { get; }
        /// <summary>
        /// Системный идентификатор пользователя (Identity ID)
        /// </summary>
        string SystemUserId { get; }

        /// <summary>
        /// Является ли пользователь библиотекарем
        /// </summary>
        /// <returns></returns>

        bool IsLibrariant();
        /// <summary>
        /// Является ли пользователь Администратором
        /// </summary>
        /// <returns></returns>
        bool IsAdmin();
    }
}
