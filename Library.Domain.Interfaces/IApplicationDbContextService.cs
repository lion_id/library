﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Library.Domain.Interfaces
{
    /// <summary>
    /// Абстракция для работы с базой данных
    /// </summary>
    public interface IApplicationDbContextService
    {
        /// <summary>
        /// Получение определенного объекта
        /// </summary>
        /// <typeparam name="T">Тип объекта</typeparam>
        DbSet<T> Set<T>() where T : class;
        /// <summary>
        /// Добавление новую запись в базу данных
        /// </summary>
        /// <typeparam name="T">Тип объекта</typeparam>
        /// <param name="entity">Сущность</param>
        /// <param name="cancellationToken">Токен для отмены запроса в случае ошибки</param>
        Task AddAsync<T>(T entity, CancellationToken cancellationToken = default) where T : class;
        /// <summary>
        /// Сохранение данных
        /// </summary>
        /// <param name="cancellationToken">Токен для отмены запроса в случае ошибки</param>
        /// <returns></returns>
        Task<int> SaveChangesAsync(CancellationToken cancellationToken = default);
        /// <summary>
        /// Транзакция
        /// </summary>
        /// <param name="action">Выполняемое действие внутри транзакции</param>
        /// <param name="cancellationToken">Токен для отмены запроса в случае ошибки</param>
        Task InvokeTransactionAsync(Func<Task> action, CancellationToken cancellationToken = default);
        /// <summary>
        /// Транзакция в случае если нужно вернуть какой либо объект
        /// </summary>
        /// <typeparam name="T">Тип объекта</typeparam>
        /// <param name="action">Выполняемое действие внутри транзакции</param>
        /// <param name="cancellationToken">Токен для отмены запроса в случае ошибки</param>
        /// <returns></returns>
        Task<T> InvokeTransactionAsync<T>(Func<Task<T>> action, CancellationToken cancellationToken = default);
    }
}
