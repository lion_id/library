﻿using Library.Domain.Interfaces;
using Library.Infrastructure.Business.Common.Extensions;
using Microsoft.AspNetCore.Http;

namespace Library.Services
{
    public class CurrentUserService : ICurrentUserService
    {
        private readonly IHttpContextAccessor httpContextAccessor;

        public CurrentUserService(IHttpContextAccessor httpContextAccessor)
        {
            this.httpContextAccessor = httpContextAccessor;
        }

        public int UserId => GetUserId();

        public string SystemUserId => GetIdentityUserId();


        private int GetUserId()
        {
            return httpContextAccessor.HttpContext?.User?.UserId() ?? 0;
        }

        private string GetIdentityUserId()
        {
            return httpContextAccessor.HttpContext?.User?.IdentityUserId();
        }

        public bool IsLibrariant()
        {
            return (bool)(httpContextAccessor.HttpContext?.User?.IdentityRoleLibrariant());
        }

        public bool IsAdmin()
        {
            return (bool)(httpContextAccessor.HttpContext?.User?.IdentityRoleAdmin());
        }
    }
}
