using Library.Infrastructure.Business.Modules.Admin.Clients.Commands.UpdateUser;
using Library.Infrastructure.Business.Modules.Admin.Clients.Queries.GetUserById;
using Library.Infrastructure.Security.Policys;
using Library.Models;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System.Threading.Tasks;

namespace Library.Pages.Administration
{
    [Authorize(Policy.ADMIN)]
    public class EditModel : PageModel
    {
        private readonly IMediator mediator;

        public EditModel(IMediator mediator)
        {
            this.mediator = mediator;
        }
        [BindProperty]
        public int Id { get; set; }

        [BindProperty]
        public UserViewModel UserViewModel { get; set; } = new UserViewModel();

        public async Task OnGetAsync(int id)
        {
            var user = await mediator.Send(new GetUserByIdQuery(id));
            UserViewModel.Id = user.Id;
            UserViewModel.FirstName = user.FirstName;
            UserViewModel.LastName = user.LastName;
            UserViewModel.MiddleName = user.MiddleName;
            UserViewModel.Email = user.Email;
            UserViewModel.Phone = user.Phone;
            UserViewModel.Role = user.Role;
            UserViewModel.UserName = user.UserName;
            UserViewModel.StatusUser = user.StatusUser;
        }

        public async Task<IActionResult> OnPostAsync(int id)
        {
            if ( id == UserViewModel.Id)
            {
                var userCommand = new UpdateUserCommand()
                {
                    Id = UserViewModel.Id.Value,
                    Email = UserViewModel.Email,
                    FirstName = UserViewModel.FirstName,
                    LastName = UserViewModel.LastName,
                    MiddleName = UserViewModel.MiddleName,
                    Password = UserViewModel.Password,
                    ConfirmPassword = UserViewModel.ConfirmPassword,
                    Phone = UserViewModel.Phone,
                    Role = UserViewModel.Role,
                    UserName = UserViewModel.UserName,
                    StatusUser = UserViewModel.StatusUser
            };
                await mediator.Send(userCommand);
                return RedirectToPage("./Index");
            }
            return BadRequest();
        }
    }
}