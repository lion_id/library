using Library.Domain;
using Library.Infrastructure.Business.Modules.Admin.Clients.Commands.BlockUser;
using Library.Infrastructure.Security.Policys;
using Library.Models;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System;
using System.Threading.Tasks;

namespace Library.Pages.Administration
{
    [Authorize(Policy.ADMIN)]
    public class BlockModel : PageModel
    {
        private readonly IMediator mediator;

        public BlockModel(IMediator mediator)
        {
            this.mediator = mediator;
        }
        [BindProperty]
        public DateTime DateBlocking { get; set; } = SystemTime.Now();
        [BindProperty]
        public int Id { get; set; }
        [BindProperty]
        public UserViewModel UserViewModel { get; set; } = new UserViewModel();
        public void OnGet(int id)
        {
            Id = id;
        }

        public async Task<IActionResult> OnPostAsync(int id)
        {
            if(id != 0)
            {

                await mediator.Send(new BlockUserCommand(Id)
                {
                    DateBlocking = DateBlocking
                });
                return RedirectToPage("./Index");
            }
            return BadRequest();
            
        }
    }
}