using Library.Infrastructure.Business.Modules.Admin.Clients.Commands.CreateUser;
using Library.Infrastructure.Business.Modules.Admin.Clients.Commands.RemoveUser;
using Library.Infrastructure.Business.Modules.Admin.Clients.Commands.UpdateUser;
using Library.Infrastructure.Business.Modules.Admin.Clients.Models;
using Library.Infrastructure.Business.Modules.Admin.Clients.Queries.GetAllUsers;
using Library.Infrastructure.Security.Policys;
using Library.Models;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Library.Pages.Administration
{
    [Authorize(Policy.ADMIN)]
    public class IndexModel : PageModel
    {
        private readonly IMediator mediator;

        public IndexModel(IMediator mediator)
        {
            this.mediator = mediator;
        }


        [BindProperty]
        public UserViewModel RegistrationViewModel { get; set; }

        [BindProperty]
        public IEnumerable<UserDto> Users { get; set; }
        public async Task OnGet()
        {

            Users = await mediator.Send(new GetAllUsersQuery());
        }

        public async Task<IActionResult> OnPost()
        {
            if (ModelState.IsValid)
            {
                var userCommand = new CreateUserCommand()
                {
                    ConfirmPassword = RegistrationViewModel.ConfirmPassword,
                    Email = RegistrationViewModel.Email,
                    FirstName = RegistrationViewModel.FirstName,
                    LastName = RegistrationViewModel.LastName,
                    MiddleName = RegistrationViewModel.MiddleName,
                    Password = RegistrationViewModel.Password,
                    Phone = RegistrationViewModel.Phone,
                    Role = RegistrationViewModel.Role,
                    UserName = RegistrationViewModel.UserName,
                };
                await mediator.Send(userCommand);
                return RedirectToPage("./Index");
            }
            return BadRequest();
        }




    }
}