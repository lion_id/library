using Library.Infrastructure.Business.Modules.Admin.Clients.Queries.GetUserById;
using Library.Infrastructure.Security.Policys;
using Library.Models;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System.Threading.Tasks;

namespace Library.Pages.Administration
{
    [Authorize(Policy.ADMIN)]
    public class DetailsModel : PageModel
    {
        private readonly IMediator mediator;

        public DetailsModel(IMediator mediator)
        {
            this.mediator = mediator;
        }

        public int Id { get; set; }

        [BindProperty]
        public UserViewModel UserViewModel { get; set; } = new UserViewModel();
        public async Task OnGet(int id)
        {
            var user = await mediator.Send(new GetUserByIdQuery(id));
            UserViewModel.Id = user.Id;
            UserViewModel.FirstName = user.FirstName;
            UserViewModel.LastName = user.LastName;
            UserViewModel.MiddleName = user.MiddleName;
            UserViewModel.Email = user.Email;
            UserViewModel.Phone = user.Phone;
            UserViewModel.Role = user.Role;
            UserViewModel.UserName = user.UserName;
            UserViewModel.IsDetails = true;
        }
    }
}