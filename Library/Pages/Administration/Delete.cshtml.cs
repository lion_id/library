using Library.Infrastructure.Business.Modules.Admin.Clients.Commands.RemoveUser;
using Library.Infrastructure.Security.Policys;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System.Threading.Tasks;

namespace Library.Pages.Administration
{
    [Authorize(Policy.ADMIN)]
    public class DeleteModel : PageModel
    {
        private readonly IMediator mediator;

        public DeleteModel(IMediator mediator)
        {
            this.mediator = mediator;
        }
        public int Id { get; set; }

        public async Task<IActionResult> OnPostRemove(int id)
        {
            if (id != 0)
            {
                await mediator.Send(new RemoveUserCommand(id));
                return RedirectToPage("./Index");
            }
            return BadRequest();
        }
    }
}