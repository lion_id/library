using FluentValidation;
using Library.Domain.Interfaces;
using Library.Infrastructure.Business;
using Library.Infrastructure.Localization;
using Library.Infrastructure.Security;
using Library.Inrfrastructure.Identity;
using Library.Options;
using Library.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Persistence;

namespace Library
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddRazorPages();
            services.AddLibraryInfrastructureBusinessCore()
                .AddLibraryPersistence(Configuration)
                .AddLibraryIdentity(Configuration)
                .AddLibrarySecurity()
                .AddLibraryLocalization();

            services.AddHttpContextAccessor();

            services.AddTransient<ICurrentUserService, CurrentUserService>();
            services.AddTransient<IEmailSenderService, EmailSenderService>();
            services.Configure<EmailOptions>(Configuration.GetSection("EmailOptions"));

            services.AddValidatorsFromAssembly(typeof(Infrastructure.Business.DependencyInjection).Assembly);
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapRazorPages();
            });
        }
    }
}
