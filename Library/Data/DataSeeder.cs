﻿using Library.Domain.Entities;
using Library.Inrfrastructure.Identity;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Persistence;
using System;
using System.Threading.Tasks;

namespace Library.Data
{
    public class DataSeeder
    {
        private readonly AppDbContext dbContext;
        private readonly UserManager<IdentityUser> userManager;
        private readonly RoleManager<IdentityRole> roleManager;

        public DataSeeder( AppDbContext DbContext, UserManager<IdentityUser> userManager, RoleManager<IdentityRole> roleManager)
        {
            this.dbContext = DbContext;
            this.userManager = userManager;
            this.roleManager = roleManager;
        }

        public async Task SeedData()
        {
            await dbContext.InvokeTransactionAsync(async () =>
            {
                await InitRoles();
                await CreateSystemUser();
            });
        }

        private async Task InitRoles()
        {
            
            foreach (var role in Enum.GetNames(typeof(Role)))
            {
                if (!await roleManager.Roles.AnyAsync(p => p.Name == role))
                {
                    IdentityRole identityRole = new()
                    {
                        Name = role
                    };
                    await roleManager.CreateAsync(identityRole);
                }
            }
        }

        private async Task CreateSystemUser()
        {
            const string password = "Password1!";
            const string name = "admin";

            if (!await userManager.Users.AnyAsync(p => p.UserName == name))
            {
                var applicationUser = await CreateIdentityUser(name, password);

                await userManager.AddToRoleAsync(applicationUser, Role.Admin.ToString());
            }
        }


        /// <summary>
        /// Создание пользователя для авторизации
        /// </summary>
        /// <param name="name"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        private async Task<IdentityUser> CreateIdentityUser(string name, string password)
        {
            var applicationUser = new IdentityUser()
            {
                Email = name,
                EmailConfirmed = true,
                UserName = name
            };

            var result = await userManager.CreateAsync(applicationUser);

            if (!string.IsNullOrEmpty(password))
                await userManager.AddPasswordAsync(applicationUser, password);
            return applicationUser;
        }
    }
}
