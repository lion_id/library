﻿using Library.Inrfrastructure.Identity;
using Microsoft.EntityFrameworkCore;
using Persistence;
using System.Threading.Tasks;

namespace Library.Data
{
    public class DatabaseMigrator
    {
        private readonly LibraryIdentityDbContext identityDbContext;
        private readonly AppDbContext appDbContext;

        public DatabaseMigrator(LibraryIdentityDbContext identityDbContext, AppDbContext appDbContext)
        {
            this.identityDbContext = identityDbContext;
            this.appDbContext = appDbContext;
        }
        public async Task MigrateAsync()
        {
            await identityDbContext.Database.MigrateAsync();
            await appDbContext.Database.MigrateAsync();
        }
    }
}
