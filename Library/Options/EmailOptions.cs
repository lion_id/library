﻿namespace Library.Options
{
	/// <summary>
	/// Опции для получение данных с json файла
	/// </summary>
	public class EmailOptions
	{
		/// <summary>
		/// Отправитель
		/// </summary>
		public string Sender { get; set; }
		/// <summary>
		/// Тип сервера (smtp.mail.ru)
		/// </summary>
		public string SmtpServer { get; set; }
		/// <summary>
		/// Порт
		/// </summary>
		public int Port { get; set; }
		/// <summary>
		/// Ссл
		/// </summary>
		public bool UseSsl { get; set; }
		/// <summary>
		/// Логин
		/// </summary>
		public string UserName { get; set; }
		/// <summary>
		/// Пароль
		/// </summary>
		public string Password { get; set; }
	}
}
