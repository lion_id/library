﻿using Library.Domain.Entities;
using Library.Domain.Entities.Enums;
using System.ComponentModel.DataAnnotations;

namespace Library.Models
{
    public class UserViewModel
    {
        public int? Id { get; set; }
        public string FirstName { get; set; }
        /// <summary>
        /// Фамилия
        /// </summary>
        public string LastName { get; set; }
        /// <summary>
        /// Отчество
        /// </summary>
        public string MiddleName { get; set; }
        /// <summary>
        /// Телефонный номер
        /// </summary>
        public string Phone { get; set; }
        public string UserName { get; set; }
        /// <summary>
        /// Логин
        /// </summary>
        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} and at max {1} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
        /// <summary>
        /// Роль
        /// </summary>
        public Role Role { get; set; }
        /// <summary>
        /// Статус пользователя
        /// </summary>
        public StatusUser StatusUser { get; set; }
        /// <summary>
        /// Просмотр
        /// </summary>
        public bool IsDetails { get; set; }
    }
}
