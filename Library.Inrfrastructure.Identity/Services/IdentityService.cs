﻿using FluentValidation.Results;
using Library.Domain;
using Library.Domain.Interfaces;
using Library.Infrastructure.Business.Common.Exceptions;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;

namespace Library.Inrfrastructure.Identity.Services
{
    public class IdentityService : IIdentityService
    {
        private readonly UserManager<IdentityUser> userManager;
        private readonly ILogger<IdentityService> logger;
        public IdentityService(UserManager<IdentityUser> userManager, ILogger<IdentityService> logger)
        {
            this.userManager = userManager;
            this.logger = logger;
        }

        public async Task<string> CreateUserAsync(string userName, string email, string password, bool needChangePassword = true, CancellationToken cancellationToken = default)
        {
            var user = new IdentityUser
            {
                UserName = userName,
                Email = email,
                EmailConfirmed = !string.IsNullOrEmpty(email),
            };
            var result = await userManager.CreateAsync(user, password);
            ThrowExceptionIfNotSuccess(result);

            return user.Id;
        }

        public async Task AddToClaimAsync(string userId, string claimType, string claimValue, CancellationToken cancellationToken = default)
        {
            var user = await userManager.FindByIdAsync(userId);
            ThrowExceptionIfNull(userId, user);
            var claims = await userManager.GetClaimsAsync(user);
            if (!claims.Any(p => p.Type == claimType))
            {
                var result = await userManager.AddClaimAsync(user, new Claim(claimType, claimValue));
                ThrowExceptionIfNotSuccess(result);
            }
        }

        public async Task AddToClaimAsync(string userId, string claimType, int claimValue, CancellationToken cancellationToken = default)
        {
            await AddToClaimAsync(userId, claimType, claimValue.ToString(), cancellationToken);
        }

        public async Task LockUserAsync(string userId, DateTimeOffset? until, CancellationToken cancellationToken = default)
        {
            var user = await userManager.FindByIdAsync(userId);
            ThrowExceptionIfNull(userId, user);
            var result = await userManager.SetLockoutEnabledAsync(user, true);
            ThrowExceptionIfNotSuccess(result);
            var lockResult = await userManager.SetLockoutEndDateAsync(user, until);
            ThrowExceptionIfNotSuccess(lockResult);
        }

        public async Task UpdateLockoutInfo(string userId, DateTimeOffset? until, bool lockoutEnabled, CancellationToken cancellationToken = default)
        {
            var user = await userManager.FindByIdAsync(userId);
            ThrowExceptionIfNull(userId, user);
            var result = await userManager.SetLockoutEnabledAsync(user, lockoutEnabled);
            ThrowExceptionIfNotSuccess(result);

            logger.LogInformation($"For {user.UserName} lockout is enabled: {lockoutEnabled}.");

            if (lockoutEnabled)
            {
                var lockResult = await userManager.SetLockoutEndDateAsync(user, until);
                ThrowExceptionIfNotSuccess(lockResult);

                logger.LogInformation($"{user.UserName} is locked till {until}.");
            }
        }

        public async Task UnlockUserAsync(string userId, CancellationToken cancellationToken = default)
        {
            var user = await userManager.FindByIdAsync(userId);
            ThrowExceptionIfNull(userId, user);
            var result = await userManager.SetLockoutEndDateAsync(user, DateTimeOffset.Now.AddDays(-1));
            ThrowExceptionIfNotSuccess(result);
            logger.LogInformation($"{user.UserName} is unlocked");
        }

        public async Task<(DateTimeOffset? lockoutEnd, bool isLockedout, bool isEnabled)> GetLockoutInfo(string userId, CancellationToken cancellationToken = default)
        {
            var user = await userManager.FindByIdAsync(userId);
            ThrowExceptionIfNull(userId, user);
            return (user.LockoutEnd, user.LockoutEnabled && user.LockoutEnd > SystemTime.Now(), user.LockoutEnabled);
        }

        public async Task ChangeUserPassword(string userId, string newPassword, CancellationToken cancellationToken = default)
        {
            var user = await userManager.FindByIdAsync(userId);
            ThrowExceptionIfNull(userId, user);
            var result = await userManager.RemovePasswordAsync(user);
            ThrowExceptionIfNotSuccess(result);
            var setPasswordResult = await userManager.AddPasswordAsync(user, newPassword);
            ThrowExceptionIfNotSuccess(setPasswordResult);
        }

        public async Task SetConfirmedEmailAsync(string userId, string email)
        {
            var user = await userManager.FindByIdAsync(userId);
            ThrowExceptionIfNull(userId, user);
            user.Email = email;
            user.EmailConfirmed = true;
            var result = await userManager.UpdateAsync(user);
            ThrowExceptionIfNotSuccess(result);
        }

        public async Task UpdateUsernameAsync(string userId, string userName)
        {
            var user = await userManager.FindByIdAsync(userId);
            ThrowExceptionIfNull(userId, user);
            var result = await userManager.SetUserNameAsync(user, userName);
            ThrowExceptionIfNotSuccess(result);
        }

        public async Task SetRole(string userId, string role)
        {
            var user = await userManager.FindByIdAsync(userId);
            ThrowExceptionIfNull(userId, user);
            var result = await userManager.AddToRoleAsync(user, role);
            ThrowExceptionIfNotSuccess(result);
        }

        public async Task RemoveFromRole(string userId, string role)
        {
            var user = await userManager.FindByIdAsync(userId);
            ThrowExceptionIfNull(userId, user);
            var result = await userManager.RemoveFromRoleAsync(user, role);
            ThrowExceptionIfNotSuccess(result);
        }

        public async Task RemoveUser(string userId)
        {
            var user = await userManager.FindByIdAsync(userId);
            ThrowExceptionIfNull(userId, user);
            var result = await userManager.DeleteAsync(user);
            ThrowExceptionIfNotSuccess(result);
        }

        private static void ThrowExceptionIfNull(string userId, IdentityUser user)
        {
            if (user == null)
            {
                throw new NotFoundException(nameof(IdentityUser), userId);
            }
        }

        public static void ThrowExceptionIfNotSuccess(IdentityResult identityResult)
        {
            if (!identityResult.Succeeded)
            {
                throw new ValidationException(identityResult.Errors.Select(prop => new ValidationFailure(prop.Code, prop.Description)));
            }
        }


    }
}
