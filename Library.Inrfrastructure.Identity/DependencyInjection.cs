﻿using Library.Domain.Interfaces;
using Library.Inrfrastructure.Identity.Services;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Library.Inrfrastructure.Identity
{
    public static class DependencyInjection
    {
        public static IServiceCollection AddLibraryIdentity(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<LibraryIdentityDbContext>((sp, options) =>
            {
                options.UseNpgsql(configuration.GetConnectionString("DefaultConnection"));

            });

            services.AddTransient<IIdentityService, IdentityService>();


            services.AddDefaultIdentity<IdentityUser>()
                .AddRoles<IdentityRole>()
                .AddEntityFrameworkStores<LibraryIdentityDbContext>()
                .AddDefaultTokenProviders();


            return services;
        }
    }
}
