﻿using Library.Infrastructure.Business.Interfaces;
using Library.Infrastructure.Localization.Resources;

namespace Library.Infrastructure.Localization
{
    public class LocalizationService : ILocalizationService
    {
        public string this[string key] => GetResource(key);

        public string GetResource(string key)
        {
            return Messages.ResourceManager.GetString(key) ?? key;
        }

    }
}
