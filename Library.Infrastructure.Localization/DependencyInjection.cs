﻿using Library.Infrastructure.Business.Interfaces;
using Microsoft.Extensions.DependencyInjection;

namespace Library.Infrastructure.Localization
{
    public static class DependencyInjection
    {
        public static IServiceCollection AddLibraryLocalization(this IServiceCollection services)
        {
            services.AddSingleton<ILocalizationService, LocalizationService>();

            return services;
        }
    }
}
