﻿using Library.Domain.Entities;
using Library.Infrastructure.Security.Policys;
using Microsoft.AspNetCore.Authorization;

namespace Library.Infrastructure.Security.Extensions
{
    public static class AuthorizationOptionsExtensions
    {
        public static void AddSecurityPolicies(this AuthorizationOptions options)
        {
            options.AddAdminPolicy();
            options.AddLibrarianPolicy();
            options.AddClientPolicy();
        }

        private static void AddAdminPolicy(this AuthorizationOptions options)
        {
            options.AddPolicy(Policy.ADMIN,
                builder => builder.RequireRole(Role.Admin.ToString()));
        }
        private static void AddLibrarianPolicy(this AuthorizationOptions options)
        {
            options.AddPolicy(Policy.LIBRARIAN,
                builder => builder.RequireRole(Role.Librarian.ToString()));
        }
        private static void AddClientPolicy(this AuthorizationOptions options)
        {
            options.AddPolicy(Policy.CLIENT,
                builder => builder.RequireRole(Role.Client.ToString()));
        }
    }
}
