﻿using Library.Infrastructure.Security.Extensions;
using Microsoft.Extensions.DependencyInjection;

namespace Library.Infrastructure.Security
{
    public static class DependencyInjection
    {
        public static IServiceCollection AddLibrarySecurity(this IServiceCollection services)
        {
            services.AddAuthorization(config => config.AddSecurityPolicies());

            return services;
        }
    }
}
