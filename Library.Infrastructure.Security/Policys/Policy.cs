﻿namespace Library.Infrastructure.Security.Policys
{
    /// <summary>
    /// Политики
    /// </summary>
    public static class Policy
    {
        /// <summary>
        /// Администратор
        /// </summary>
        public const string ADMIN = "Администратор";
        /// <summary>
        /// Библиотекарь
        /// </summary>
        public const string LIBRARIAN = "Библиотекарь";
        /// <summary>
        /// Клиент
        /// </summary>
        public const string CLIENT = "Клиент";
    }
}
